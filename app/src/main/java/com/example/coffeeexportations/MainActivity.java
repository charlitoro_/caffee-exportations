package com.example.coffeeexportations;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Pie;
import com.example.coffeeexportations.CoffeeApi.CoffeeApi;
import com.example.coffeeexportations.Models.Reports;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    // Year picker
    Button btnYear;
    Integer year = Calendar.getInstance().get(Calendar.YEAR) - 1 ;;

    // Retrofit request
    private Retrofit retrofit;
    private List<Reports> reports;
    private final static String LOGS = "---| ";

    // Any Chart View
    AnyChartView anyChartView;;
    ArrayList<String> departments = new ArrayList<String>();
    ArrayList<Double> tons = new ArrayList<Double>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // btnYear
        btnYear = findViewById(R.id.btnYear);
        btnYear.setText(year.toString());

        // Retroview get data
        retrofit = new Retrofit.Builder().baseUrl("https://www.datos.gov.co/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // any chart view
        anyChartView = findViewById(R.id.anyChartView);

        getData();
    }

    //---------------------------------------------------------------------------------
    // Dialog Year Picker
    public void showYearDialog(View view) {
        final Dialog dialogYear = new Dialog(MainActivity.this);
        dialogYear.setTitle("Year Picker");
        dialogYear.setContentView(R.layout.year_dialog);
        Button btnSet = dialogYear.findViewById(R.id.btnSet);
        Button btnCancel = dialogYear.findViewById(R.id.btnCancel);
        TextView year_text = dialogYear.findViewById(R.id.year_text);
        year_text.setText(year.toString());
        final NumberPicker nopicker = dialogYear.findViewById(R.id.yearPicker);

        nopicker.setMaxValue(year+50);
        nopicker.setMinValue(year-50);
        nopicker.setWrapSelectorWheel(false);
        nopicker.setValue(year);
        nopicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnYear.setText(String.valueOf(nopicker.getValue()));
                year = nopicker.getValue();
                departments.clear();
                tons.clear();
                getData();
                dialogYear.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogYear.dismiss();
            }
        });
        dialogYear.show();
    }

    // -----------------------------------------------------------------------
    // Retrofit Request
    public void setupPieChart() {
        Pie pie = AnyChart.pie();
        List<DataEntry> dataEntries = new ArrayList<>();
        for (int i = 0; i < departments.size(); i++) {
            dataEntries.add(new ValueDataEntry(departments.get(i), tons.get(i)));
        }
        pie.data(dataEntries);
        anyChartView.setChart(pie);
    }

    // -----------------------------------------------------------------------
    // Retrofit Request
    private void getData() {
        try {
            CoffeeApi service = retrofit.create(CoffeeApi.class);
            Call<List<Reports>> call = service.getReports("gzwq-vje7.json?anio="+year.toString());

            call.enqueue(new Callback<List<Reports>>() {
                @Override
                public void onResponse(Call<List<Reports>> call, Response<List<Reports>> response) {
                    if (response.isSuccessful()) {
                        reports = response.body();
                        Toast.makeText(MainActivity.this, "# Reports: "+reports.size(), Toast.LENGTH_SHORT).show();
                        for(int i=0; i < reports.size(); i++) {
                            Reports report = reports.get(i);
                            if (departments.contains(report.getDepartamentoorigen())) {
                                int index = departments.indexOf(report.getDepartamentoorigen());
                                tons.set(index, tons.get(index) + report.getVolumentoneladas());
                            } else {
                                departments.add(report.getDepartamentoorigen());
                                tons.add(report.getVolumentoneladas());
                            }
                        }
                        setupPieChart();
                    } else {
                        Log.e(LOGS, "onResponse: " + response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<List<Reports>> call, Throwable t) {
                    Log.e(LOGS, "onFailure: " + t);
                }
            });
        } catch (Exception e) {
            Log.e(LOGS, "onFailure: " + e);
        }
    }
}
