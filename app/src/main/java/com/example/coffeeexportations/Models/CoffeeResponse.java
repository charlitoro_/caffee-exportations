package com.example.coffeeexportations.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CoffeeResponse {
    @SerializedName("reports")
    @Expose
    private List<Reports> reports = null;

    public List<Reports> getReports() {
        return reports;
    }

    public void setReports(List<Reports> reports) {
        this.reports = reports;
    }
}
