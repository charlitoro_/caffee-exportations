package com.example.coffeeexportations.CoffeeApi;

import com.example.coffeeexportations.Models.Reports;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface CoffeeApi {
    @GET
    Call<List<Reports>> getReports(@Url String url);
}